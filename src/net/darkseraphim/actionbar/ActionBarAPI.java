package net.darkseraphim.actionbar;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.logging.Level;

import net.amoebaman.util.Reflection;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.EnumProtocol;
import net.minecraft.server.v1_7_R4.NetworkManager;
import net.minecraft.server.v1_7_R4.Packet;
import net.minecraft.server.v1_7_R4.PacketPlayOutChat;
import net.minecraft.util.com.google.common.collect.BiMap;
import net.minecraft.util.io.netty.channel.Channel;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.GsonBuilder;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;


public class ActionBarAPI
{
	private static final Gson GSON = new GsonBuilder().create();
    private static BiMap map;
    private static ProxyHashBiMap<Integer, Class> special;
    
    static
    {
    	overrideI();
        injectCustomPacket(PacketPlayOutChat.class, PacketPlayOutActionBar.class);
        refreshPlayers(true);
    }
    
    public static void sendActionBarRaw(String json, Player... players)
    {
    	Packet packet = new PacketPlayOutActionBar(json);
    	
    	for(Player player : players)
    		if(isProtocolVersionSufficient(player))
    		{
	    		EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
	    		
	    		entityPlayer.playerConnection.sendPacket(packet);
    		}
    		else
    			tellRaw(player, json);
    }
    
    public static void sendActionBar(String content, Player... players)
    {
    	JsonObject contentRoot = new JsonObject();
    	contentRoot.addProperty("text", content != null ? content : "");
    	
		sendActionBarRaw(GSON.toJson(contentRoot), players);
    }
    
    private static boolean isProtocolVersionSufficient(Player player)
    {
    	return getProtocolVersion(player) >= 47;
    }
    
	private static int getProtocolVersion(Player player)
	{
		try
		{
			EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
			
			return entityPlayer.playerConnection.networkManager.getVersion();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return 0;
	}
	
	private static void tellRaw(Player player, String jsonString)
	{
		try
		{
			Object handle = Reflection.getHandle(player);
			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			Reflection.getMethod(connection.getClass(), "sendPacket", Reflection.getNMSClass("Packet")).invoke(connection, createChatPacket(jsonString));
		}
		catch(IllegalArgumentException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
		}
		catch(IllegalAccessException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
		}
		catch(InstantiationException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Underlying class is abstract.", e);
		}
		catch(InvocationTargetException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
		}
	}

	private static net.minecraft.util.com.google.gson.Gson nmsChatSerializerGsonInstance;
	private static Constructor<?> nmsPacketPlayOutChatConstructor;

	private static Object createChatPacket(String json) throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException
	{
		if(nmsChatSerializerGsonInstance == null)
		{
			// Find the field and its value, completely bypassing obfuscation
			for(Field declaredField : Reflection.getNMSClass("ChatSerializer").getDeclaredFields())
			{
				if(Modifier.isFinal(declaredField.getModifiers()) && Modifier.isStatic(declaredField.getModifiers()) && declaredField.getType() == net.minecraft.util.com.google.gson.Gson.class)
				{
					// We've found our field
					declaredField.setAccessible(true);
					nmsChatSerializerGsonInstance = (net.minecraft.util.com.google.gson.Gson) declaredField.get(null);
					break;
				}
			}
		}

		// Since the method is so simple, and all the obfuscated methods have
		// the same name, it's easier to reimplement 'IChatBaseComponent
		// a(String)' than to reflectively call it
		// Of course, the implementation may change, but fuzzy matches might
		// break with signature changes
		Object serializedChatComponent = nmsChatSerializerGsonInstance.fromJson(json, Reflection.getNMSClass("IChatBaseComponent"));

		if(nmsPacketPlayOutChatConstructor == null)
		{
			try
			{
				nmsPacketPlayOutChatConstructor = Reflection.getNMSClass("PacketPlayOutChat").getDeclaredConstructor(Reflection.getNMSClass("IChatBaseComponent"));
				nmsPacketPlayOutChatConstructor.setAccessible(true);
			}
			catch(NoSuchMethodException e)
			{
				Bukkit.getLogger().log(Level.SEVERE, "Could not find Minecraft method or constructor.", e);
			}
			catch(SecurityException e)
			{
				Bukkit.getLogger().log(Level.WARNING, "Could not access constructor.", e);
			}
		}

		return nmsPacketPlayOutChatConstructor.newInstance(serializedChatComponent);
	}
    
    /*
     * Reflection magic by DarkSeraphim follows...
     */
    
    private static void overrideI()
    {
        try
        {
            Field field = EnumProtocol.class.getDeclaredField("i");
            field.setAccessible(true);
            map = (BiMap) field.get(EnumProtocol.PLAY);
            special = new ProxyHashBiMap<Integer, Class>(map);
            set(field, EnumProtocol.PLAY, special);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw new ExceptionInInitializerError("Nope, not allowed.");
        }
    }

    private static void set(Field f, Object o, Object v) throws NoSuchFieldException, IllegalAccessException
    {
        boolean accessible = f.isAccessible();
        f.setAccessible(true);
        if ((f.getModifiers() & Modifier.FINAL) != 0)
        {
            set(Field.class.getDeclaredField("modifiers"), f, f.getModifiers() & ~Modifier.FINAL);
        }
        f.set(o, v);
    }

    private static <T extends Packet> void injectCustomPacket(Class<T> mc, Class<? extends T> custom)
    {
        special.inverse().injectSpecial(PacketPlayOutActionBar.class, PacketPlayOutChat.class);
        try
        {
            Field field = EnumProtocol.class.getDeclaredField("f");
            field.setAccessible(true);
            ((Map)field.get(null)).put(custom, EnumProtocol.PLAY);
        }
        catch (NoSuchFieldException ex)
        {
            ex.printStackTrace();
        }
        catch (IllegalAccessException ex)
        {
            ex.printStackTrace();
        }
    }

    private static <T extends Packet> void ejectCustomPacket(Class<? extends T> custom)
    {
        special.inverse().ejectSpecial(custom);
        //Validate.notNull(id, "You can only eject existing packets");
        try
        {
            Field field = EnumProtocol.class.getDeclaredField("f");
            field.setAccessible(true);
            ((Map)field.get(null)).remove(custom);
        }
        catch (NoSuchFieldException ex)
        {
            ex.printStackTrace();
        }
        catch (IllegalAccessException ex)
        {
            ex.printStackTrace();
        }
    }
    
    private static void returnI()
    {
        try
        {
            Field field = EnumProtocol.class.getDeclaredField("i");
            field.setAccessible(true);
            set(field, EnumProtocol.PLAY, map);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    private static void refreshPlayers(boolean enable)
    {
        try
        {
            Field channelField = NetworkManager.class.getDeclaredField("m");
            channelField.setAccessible(true);
            Channel channel;
            for (Player player : Bukkit.getOnlinePlayers())
            {
                channel = (Channel) channelField.get(((CraftPlayer) player).getHandle().playerConnection.networkManager);
                channel.attr(NetworkManager.f).set(enable ? special : map);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}