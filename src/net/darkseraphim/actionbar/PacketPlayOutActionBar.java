package net.darkseraphim.actionbar;

import java.io.IOException;

import net.minecraft.server.v1_7_R4.ChatSerializer;
import net.minecraft.server.v1_7_R4.PacketDataSerializer;
import net.minecraft.server.v1_7_R4.PacketListener;
import net.minecraft.server.v1_7_R4.PacketPlayOutChat;
import net.minecraft.server.v1_7_R4.PacketPlayOutListener;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;


/**
 * @author DarkSeraphim
 */
public class PacketPlayOutActionBar extends PacketPlayOutChat
{
    public String json;

    /**
     * Creates an action bar message
     * @param message The message to display
     */
    public PacketPlayOutActionBar(String json)
    {
        super();
        this.json = json;
    }
    
    public void a(PacketDataSerializer packetdataserializer) throws IOException
    {
        this.json = ChatSerializer.a(ChatSerializer.a(packetdataserializer.c(32767)));
    }

    public void b(PacketDataSerializer packetdataserializer) throws IOException
    {
        packetdataserializer.a(this.json);
        packetdataserializer.writeByte(0x2);
    }

    public void a(PacketPlayOutListener packetplayoutlistener)
    {
        packetplayoutlistener.a(this);
    }

    public String b()
    {
        return String.format("actionbar='%s'", new Object[] { this.json });
    }

    public void handle(PacketListener packetlistener)
    {
        a((PacketPlayOutListener)packetlistener);
    }

    // Added some helper methods
    public void send(Player player)
    {
        if(((CraftPlayer)player).getHandle().playerConnection.networkManager.getVersion() < 47)
            return;

        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(this);
    }

    public void broadcast()
    {
        for(Player player : Bukkit.getOnlinePlayers())
        {
            send(player);
        }
    }
}