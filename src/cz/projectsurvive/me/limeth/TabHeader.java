package cz.projectsurvive.me.limeth;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import net.amoebaman.util.Reflection;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.IChatBaseComponent;
import net.minecraft.server.v1_7_R4.Packet;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.GsonBuilder;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TabHeader
{
	private static final Class<?> CLASS_PACKET_TITLE = getClass("org.spigotmc.ProtocolInjector$PacketTabHeader");
	private static final Class<?> CLASS_CHAT_SERIALIZER = getNMSClass("ChatSerializer");
	private static final Gson GSON = new GsonBuilder().create();

	private String header;
	private String footer;

	/**
	 * @param header
	 *            in json
	 * @param footer
	 *            in json
	 */
	public TabHeader(String header, String footer)
	{
		this.setHeader(header);
		this.setFooter(footer);
	}

	public TabHeader send(Player... players)
	{
		send(header, footer, players);
		return this;
	}

	public TabHeader sendRaw(Player... players)
	{
		sendRaw(header, footer, players);
		return this;
	}

	public static void sendRaw(String headerJson, String footerJson, Player... players)
	{
		try
		{
			Constructor<?> constructor = CLASS_PACKET_TITLE.getConstructor(IChatBaseComponent.class, IChatBaseComponent.class);
			Method serializationMethod = getMethod(CLASS_CHAT_SERIALIZER, "a", String.class);
			Object serializedHeader = serializationMethod.invoke(null, headerJson);
			Object serializedFooter = serializationMethod.invoke(null, footerJson);
			Packet packet = (Packet) constructor.newInstance(serializedHeader, serializedFooter);

			for(Player player : players)
			{
				try
				{
					if(!isProtocolVersionSufficient(player))
					{
						if(headerJson != null)
							tellRaw(player, headerJson);

						if(footerJson != null)
							tellRaw(player, footerJson);
						
						continue;
					}

					Object handle = getMethod("getHandle", player.getClass()).invoke(player);
					Object connection = getField(handle.getClass(), "playerConnection").get(handle);
					Method sendPacket = getMethod(connection.getClass(), "sendPacket");

					sendPacket.invoke(connection, packet);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void send(String header, String footer, Player... players)
	{
		JsonObject headerRoot = new JsonObject();
		headerRoot.addProperty("text", header != null ? header : "");
		JsonObject footerRoot = new JsonObject();
		footerRoot.addProperty("text", footer != null ? footer : "");

		sendRaw(GSON.toJson(headerRoot), GSON.toJson(footerRoot), players);
	}

	private static boolean isProtocolVersionSufficient(Player player)
	{
		return getProtocolVersion(player) >= 47;
	}

	private static int getProtocolVersion(Player player)
	{
		try
		{
			EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();

			return entityPlayer.playerConnection.networkManager.getVersion();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		return 0;
	}

	private static void tellRaw(Player player, String jsonString)
	{
		try
		{
			Object handle = Reflection.getHandle(player);
			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			Reflection.getMethod(connection.getClass(), "sendPacket", Reflection.getNMSClass("Packet")).invoke(connection, createChatPacket(jsonString));
		}
		catch(IllegalArgumentException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
		}
		catch(IllegalAccessException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
		}
		catch(InstantiationException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Underlying class is abstract.", e);
		}
		catch(InvocationTargetException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
		}
	}

	private static net.minecraft.util.com.google.gson.Gson nmsChatSerializerGsonInstance;
	private static Constructor<?> nmsPacketPlayOutChatConstructor;

	private static Object createChatPacket(String json) throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException
	{
		if(nmsChatSerializerGsonInstance == null)
		{
			// Find the field and its value, completely bypassing obfuscation
			for(Field declaredField : Reflection.getNMSClass("ChatSerializer").getDeclaredFields())
			{
				if(Modifier.isFinal(declaredField.getModifiers()) && Modifier.isStatic(declaredField.getModifiers()) && declaredField.getType() == net.minecraft.util.com.google.gson.Gson.class)
				{
					// We've found our field
					declaredField.setAccessible(true);
					nmsChatSerializerGsonInstance = (net.minecraft.util.com.google.gson.Gson) declaredField.get(null);
					break;
				}
			}
		}

		// Since the method is so simple, and all the obfuscated methods have
		// the same name, it's easier to reimplement 'IChatBaseComponent
		// a(String)' than to reflectively call it
		// Of course, the implementation may change, but fuzzy matches might
		// break with signature changes
		Object serializedChatComponent = nmsChatSerializerGsonInstance.fromJson(json, Reflection.getNMSClass("IChatBaseComponent"));

		if(nmsPacketPlayOutChatConstructor == null)
		{
			try
			{
				nmsPacketPlayOutChatConstructor = Reflection.getNMSClass("PacketPlayOutChat").getDeclaredConstructor(Reflection.getNMSClass("IChatBaseComponent"));
				nmsPacketPlayOutChatConstructor.setAccessible(true);
			}
			catch(NoSuchMethodException e)
			{
				Bukkit.getLogger().log(Level.SEVERE, "Could not find Minecraft method or constructor.", e);
			}
			catch(SecurityException e)
			{
				Bukkit.getLogger().log(Level.WARNING, "Could not access constructor.", e);
			}
		}

		return nmsPacketPlayOutChatConstructor.newInstance(serializedChatComponent);
	}

	/**
	 * @return header in json
	 */
	public String getHeader()
	{
		return header;
	}

	/**
	 * @param header
	 *            in json
	 * @return the TabHeader
	 */
	public TabHeader setHeader(String header)
	{
		this.header = header;
		return this;
	}

	/**
	 * @return footer in json
	 */
	public String getFooter()
	{
		return footer;
	}

	/**
	 * @param footer
	 *            in json
	 * @return the TabHeader
	 */
	public TabHeader setFooter(String footer)
	{
		this.footer = footer;
		return this;
	}

	private static String getVersion()
	{
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}

	private static Class<?> getNMSClass(String className)
	{
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try
		{
			clazz = Class.forName(fullName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return clazz;
	}

	private static final Map<Class<?>, Class<?>> CORRESPONDING_TYPES = new HashMap<Class<?>, Class<?>>();

	private static boolean equalsTypeArray(Class<?>[] a, Class<?>[] o)
	{
		if(a.length != o.length)
			return false;
		for(int i = 0; i < a.length; i++)
			if(!a[i].equals(o[i]) && !a[i].isAssignableFrom(o[i]))
				return false;
		return true;
	}

	private static Method getMethod(String name, Class<?> clazz, Class<?>... paramTypes)
	{
		Class<?>[] t = toPrimitiveTypeArray(paramTypes);
		for(Method m : clazz.getMethods())
		{
			Class<?>[] types = toPrimitiveTypeArray(m.getParameterTypes());
			if(m.getName().equals(name) && equalsTypeArray(types, t))
				return m;
		}
		return null;
	}

	private static Class<?> getPrimitiveType(Class<?> clazz)
	{
		return CORRESPONDING_TYPES.containsKey(clazz) ? CORRESPONDING_TYPES.get(clazz) : clazz;
	}

	private static Class<?>[] toPrimitiveTypeArray(Class<?>[] classes)
	{
		int a = classes != null ? classes.length : 0;
		Class<?>[] types = new Class<?>[a];
		for(int i = 0; i < a; i++)
			types[i] = getPrimitiveType(classes[i]);
		return types;
	}

	private static Class<?> getClass(String namespace)
	{
		try
		{
			return Class.forName(namespace);
		}
		catch(Exception e)
		{
		}

		return null;
	}

	private static Field getField(Class<?> clazz, String name)
	{
		try
		{
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Method getMethod(Class<?> clazz, String name, Class<?>... args)
	{
		for(Method m : clazz.getMethods())
			if(m.getName().equals(name) && (args.length == 0 || ClassListEqual(args, m.getParameterTypes())))
			{
				m.setAccessible(true);
				return m;
			}
		return null;
	}

	private static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2)
	{
		boolean equal = true;
		if(l1.length != l2.length)
			return false;
		for(int i = 0; i < l1.length; i++)
			if(l1[i] != l2[i])
			{
				equal = false;
				break;
			}
		return equal;
	}
}
