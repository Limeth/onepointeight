package cz.projectsurvive.me.limeth;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import net.amoebaman.util.Reflection;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.GsonBuilder;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;
import org.bukkit.entity.Player;

/**
 * Minecraft 1.8 Title
 *
 * @version 1.0.4
 * @author Maxim Van de Wynckel
 * 
 * Edited by Limeth
 */
public class Title
{
	private static final Class<?> CLASS_PACKET_TITLE = getClass("org.spigotmc.ProtocolInjector$PacketTitle");
	private static final Class<?> CLASS_PACKET_ACTION = getClass("org.spigotmc.ProtocolInjector$PacketTitle$Action");
	private static final Class<?> CLASS_CHAT_SERIALIZER = getNMSClass("ChatSerializer");
	private static final Gson GSON = new GsonBuilder().create();
	private String title = "";
	private String subtitle = "";
	private Integer fadeInTime = -1;
	private Integer stayTime = -1;
	private Integer fadeOutTime = -1;

	private static final Map<Class<?>, Class<?>> CORRESPONDING_TYPES = new HashMap<Class<?>, Class<?>>();

	/**
	 * Create a new 1.8 title
	 * 
	 * @param title
	 *            Title json
	 */
	public Title(String title)
	{
		this.title = title;
	}

	/**
	 * Create a new 1.8 title
	 * 
	 * @param title
	 *            Title json
	 * @param subtitle
	 *            Subtitle json
	 */
	public Title(String title, String subtitle)
	{
		this.title = title;
		this.subtitle = subtitle;
	}
	
	/**
	 * Copy 1.8 title
	 * 
	 * @param title
	 *            Title
	 */
	public Title(Title title)
	{
		// Copy title
		this.title = title.title;
		this.subtitle = title.subtitle;
		this.fadeInTime = title.fadeInTime;
		this.fadeOutTime = title.fadeOutTime;
		this.stayTime = title.stayTime;
	}

	/**
	 * Create a new 1.8 title
	 * 
	 * @param title
	 *            Title text
	 * @param subtitle
	 *            Subtitle text
	 * @param fadeInTime
	 *            Fade in time
	 * @param stayTime
	 *            Stay on screen time
	 * @param fadeOutTime
	 *            Fade out time
	 */
	public Title(String title, String subtitle, Integer fadeInTime, Integer stayTime, Integer fadeOutTime)
	{
		this.title = title;
		this.subtitle = subtitle;
		this.fadeInTime = fadeInTime;
		this.stayTime = stayTime;
		this.fadeOutTime = fadeOutTime;
	}

	/**
	 * Set title json
	 * 
	 * @param title
	 *            Title json
	 */
	public Title setTitle(String title)
	{
		this.title = title;
		return this;
	}

	/**
	 * Get title json
	 * 
	 * @return Title json
	 */
	public String getTitle()
	{
		return this.title;
	}

	/**
	 * Set subtitle json
	 * 
	 * @param subtitle
	 *            Subtitle text
	 */
	public Title setSubtitle(String subtitle)
	{
		this.subtitle = subtitle;
		return this;
	}

	/**
	 * Get subtitle json
	 * 
	 * @return Subtitle json
	 */
	public String getSubtitle()
	{
		return this.subtitle;
	}

	/**
	 * Set title fade in time
	 * 
	 * @param time
	 *            Time
	 */
	public Title setFadeInTime(Integer time)
	{
		this.fadeInTime = time;
		return this;
	}

	/**
	 * Set title fade out time
	 * 
	 * @param time
	 *            Time
	 */
	public Title setFadeOutTime(Integer time)
	{
		this.fadeOutTime = time;
		return this;
	}

	/**
	 * Set title stay time
	 * 
	 * @param time
	 *            Time
	 */
	public Title setStayTime(Integer time)
	{
		this.stayTime = time;
		return this;
	}

	/**
	 * Send the title to a player
	 * 
	 * @param player
	 *            Player
	 */
	public Title send(Player player)
	{
		send(player, title, subtitle, stayTime, fadeInTime, fadeOutTime);
		return this;
	}

	/**
	 * Send the raw title to a player
	 * 
	 * @param player
	 *            Player
	 */
	public Title sendRaw(Player player)
	{
		sendRaw(player, title, subtitle, stayTime, fadeInTime, fadeOutTime);
		return this;
	}
	
	public static void sendRaw(Player player, String titleJson, String subtitleJson, Integer stayTime, Integer fadeInTime, Integer fadeOutTime)
	{
		if(getProtocolVersion(player) >= 47 && isSpigot() && CLASS_PACKET_TITLE != null)
		{
			// First reset previous settings
			resetTitle(player);
			try
			{
				Object handle = getHandle(player);
				Object connection = getField(handle.getClass(), "playerConnection").get(handle);
				Object[] actions = CLASS_PACKET_ACTION.getEnumConstants();
				Method sendPacket = getMethod(connection.getClass(), "sendPacket");
				// Send timings first
				if(fadeInTime != null && fadeOutTime != null && stayTime != null)
				{
					Object packet = CLASS_PACKET_TITLE.getConstructor(CLASS_PACKET_ACTION, Integer.TYPE, Integer.TYPE, Integer.TYPE)
							.newInstance(actions[2], fadeInTime, stayTime, fadeOutTime);
					sendPacket.invoke(connection, packet);
				}

				// Send title
				if(titleJson != null && titleJson.length() > 0)
				{
					Object serialized = getMethod(CLASS_CHAT_SERIALIZER, "a", String.class).invoke(null, titleJson);
					Object packet = CLASS_PACKET_TITLE.getConstructor(CLASS_PACKET_ACTION, getNMSClass("IChatBaseComponent")).newInstance(actions[0], serialized);
					sendPacket.invoke(connection, packet);
				}
				
				// Send subtitle if present
				if(subtitleJson != null && subtitleJson.length() > 0)
				{
					Object serialized = getMethod(CLASS_CHAT_SERIALIZER, "a", String.class).invoke(null, subtitleJson);
					Object packet = CLASS_PACKET_TITLE.getConstructor(CLASS_PACKET_ACTION, getNMSClass("IChatBaseComponent")).newInstance(actions[1], serialized);
					sendPacket.invoke(connection, packet);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			if(titleJson != null)
				tellRaw(player, titleJson);
			
			if(subtitleJson != null)
				tellRaw(player, subtitleJson);
		}
	}
	
	public static void sendRaw(Player player, String titleJson, String subtitleJson)
	{
		sendRaw(player, titleJson, subtitleJson, null, null, null);
	}
	
	public static void send(Player player, String title, String subtitle, Integer stayTime, Integer fadeInTime, Integer fadeOutTime)
	{
		JsonObject titleRoot = new JsonObject();
		titleRoot.addProperty("text", title != null ? title : "");
		JsonObject subtitleRoot = new JsonObject();
		subtitleRoot.addProperty("text", subtitle != null ? subtitle : "");
		
		sendRaw(player, GSON.toJson(titleRoot), GSON.toJson(subtitleRoot), stayTime, fadeInTime, fadeOutTime);
	}
	
	public static void send(Player player, String title, String subtitle)
	{
		send(player, title, subtitle, null, null, null);
	}
	
	private static void tellRaw(Player player, String jsonString)
	{
		try
		{
			Object handle = Reflection.getHandle(player);
			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			Reflection.getMethod(connection.getClass(), "sendPacket", Reflection.getNMSClass("Packet")).invoke(connection, createChatPacket(jsonString));
		}
		catch(IllegalArgumentException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Argument could not be passed.", e);
		}
		catch(IllegalAccessException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Could not access method.", e);
		}
		catch(InstantiationException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "Underlying class is abstract.", e);
		}
		catch(InvocationTargetException e)
		{
			Bukkit.getLogger().log(Level.WARNING, "A error has occured durring invoking of method.", e);
		}
	}

	private static net.minecraft.util.com.google.gson.Gson nmsChatSerializerGsonInstance;
	private static Constructor<?> nmsPacketPlayOutChatConstructor;

	private static Object createChatPacket(String json) throws IllegalArgumentException, IllegalAccessException, InstantiationException, InvocationTargetException
	{
		if(nmsChatSerializerGsonInstance == null)
		{
			// Find the field and its value, completely bypassing obfuscation
			for(Field declaredField : Reflection.getNMSClass("ChatSerializer").getDeclaredFields())
			{
				if(Modifier.isFinal(declaredField.getModifiers()) && Modifier.isStatic(declaredField.getModifiers()) && declaredField.getType() == net.minecraft.util.com.google.gson.Gson.class)
				{
					// We've found our field
					declaredField.setAccessible(true);
					nmsChatSerializerGsonInstance = (net.minecraft.util.com.google.gson.Gson) declaredField.get(null);
					break;
				}
			}
		}

		// Since the method is so simple, and all the obfuscated methods have
		// the same name, it's easier to reimplement 'IChatBaseComponent
		// a(String)' than to reflectively call it
		// Of course, the implementation may change, but fuzzy matches might
		// break with signature changes
		Object serializedChatComponent = nmsChatSerializerGsonInstance.fromJson(json, Reflection.getNMSClass("IChatBaseComponent"));

		if(nmsPacketPlayOutChatConstructor == null)
		{
			try
			{
				nmsPacketPlayOutChatConstructor = Reflection.getNMSClass("PacketPlayOutChat").getDeclaredConstructor(Reflection.getNMSClass("IChatBaseComponent"));
				nmsPacketPlayOutChatConstructor.setAccessible(true);
			}
			catch(NoSuchMethodException e)
			{
				Bukkit.getLogger().log(Level.SEVERE, "Could not find Minecraft method or constructor.", e);
			}
			catch(SecurityException e)
			{
				Bukkit.getLogger().log(Level.WARNING, "Could not access constructor.", e);
			}
		}

		return nmsPacketPlayOutChatConstructor.newInstance(serializedChatComponent);
	}

	/**
	 * Broadcast the title to all players
	 */
	public Title broadcast()
	{
		for(Player p : Bukkit.getOnlinePlayers())
			send(p);
		
		return this;
	}
	
	/*
	 * Static fields below
	 */
	
	/**
	 * Clear the title
	 * 
	 * @param player
	 *            Player
	 */
	public static void clearTitle(Player player)
	{
		if(getProtocolVersion(player) >= 47 && isSpigot())
		{
			try
			{
				// Send timings first
				Object handle = getHandle(player);
				Object connection = getField(handle.getClass(), "playerConnection").get(handle);
				Object[] actions = CLASS_PACKET_ACTION.getEnumConstants();
				Method sendPacket = getMethod(connection.getClass(), "sendPacket");
				Object packet = CLASS_PACKET_TITLE.getConstructor(CLASS_PACKET_ACTION).newInstance(actions[3]);
				sendPacket.invoke(connection, packet);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Reset the title settings
	 * 
	 * @param player
	 *            Player
	 */
	public static void resetTitle(Player player)
	{
		if(getProtocolVersion(player) >= 47 && isSpigot())
		{
			try
			{
				// Send timings first
				Object handle = getHandle(player);
				Object connection = getField(handle.getClass(), "playerConnection").get(handle);
				Object[] actions = CLASS_PACKET_ACTION.getEnumConstants();
				Method sendPacket = getMethod(connection.getClass(), "sendPacket");
				Object packet = CLASS_PACKET_TITLE.getConstructor(CLASS_PACKET_ACTION).newInstance(actions[4]);
				sendPacket.invoke(connection, packet);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Get the protocol version of the player
	 * 
	 * @param player
	 *            Player
	 * @return Protocol version
	 */
	private static int getProtocolVersion(Player player)
	{
		int version = 0;
		try
		{
			Object handle = getHandle(player);
			Object connection = getField(handle.getClass(), "playerConnection").get(handle);
			Object networkManager = getValue("networkManager", connection);
			version = (Integer) getMethod("getVersion", networkManager.getClass()).invoke(networkManager);

			return version;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return version;
	}

	/**
	 * Check if running spigot
	 * 
	 * @return Spigot
	 */
	private static boolean isSpigot()
	{
		return Bukkit.getVersion().contains("Spigot");
	}
	
	/**
	 * Get class by url
	 * 
	 * @param namespace
	 *            Namespace url
	 * @return Class
	 */
	private static Class<?> getClass(String namespace)
	{
		try
		{
			return Class.forName(namespace);
		}
		catch(Exception e)
		{

		}
		return null;
	}

	private static Field getField(String name, Class<?> clazz) throws Exception
	{
		return clazz.getDeclaredField(name);
	}

	private static Object getValue(String name, Object obj) throws Exception
	{
		Field f = getField(name, obj.getClass());
		f.setAccessible(true);
		return f.get(obj);
	}

	private static Class<?> getPrimitiveType(Class<?> clazz)
	{
		return CORRESPONDING_TYPES.containsKey(clazz) ? CORRESPONDING_TYPES.get(clazz) : clazz;
	}

	private static Class<?>[] toPrimitiveTypeArray(Class<?>[] classes)
	{
		int a = classes != null ? classes.length : 0;
		Class<?>[] types = new Class<?>[a];
		for(int i = 0; i < a; i++)
			types[i] = getPrimitiveType(classes[i]);
		return types;
	}

	private static boolean equalsTypeArray(Class<?>[] a, Class<?>[] o)
	{
		if(a.length != o.length)
			return false;
		for(int i = 0; i < a.length; i++)
			if(!a[i].equals(o[i]) && !a[i].isAssignableFrom(o[i]))
				return false;
		return true;
	}

	private static Object getHandle(Object obj)
	{
		try
		{
			return getMethod("getHandle", obj.getClass()).invoke(obj);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Method getMethod(String name, Class<?> clazz, Class<?>... paramTypes)
	{
		Class<?>[] t = toPrimitiveTypeArray(paramTypes);
		for(Method m : clazz.getMethods())
		{
			Class<?>[] types = toPrimitiveTypeArray(m.getParameterTypes());
			if(m.getName().equals(name) && equalsTypeArray(types, t))
				return m;
		}
		return null;
	}

	private static String getVersion()
	{
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}

	private static Class<?> getNMSClass(String className)
	{
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try
		{
			clazz = Class.forName(fullName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return clazz;
	}
	
	private static Field getField(Class<?> clazz, String name)
	{
		try
		{
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Method getMethod(Class<?> clazz, String name, Class<?>... args)
	{
		for(Method m : clazz.getMethods())
			if(m.getName().equals(name) && (args.length == 0 || ClassListEqual(args, m.getParameterTypes())))
			{
				m.setAccessible(true);
				return m;
			}
		return null;
	}

	private static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2)
	{
		boolean equal = true;
		if(l1.length != l2.length)
			return false;
		for(int i = 0; i < l1.length; i++)
			if(l1[i] != l2[i])
			{
				equal = false;
				break;
			}
		return equal;
	}
}