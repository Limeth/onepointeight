package cz.projectsurvive.me.limeth;

import net.darkseraphim.actionbar.ActionBarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class OnePointEight extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		Bukkit.getPluginCommand("ope").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length <= 0)
		{
			sender.sendMessage("bar, tab, title");
			return true;
		}
		else if(args[0].equalsIgnoreCase("bar"))
		{
			if(args.length < 2)
			{
				sender.sendMessage("Usage: /ope bar [Content]");
				return true;
			}
			
			String rawContent = args[1];
			
			for(int i = 2; i < args.length; i++)
				rawContent += " " + args[i];
			
			rawContent = ChatColor.translateAlternateColorCodes('&', rawContent);
			
			ActionBarAPI.sendActionBar(rawContent, (Player) sender);
			sender.sendMessage("Sent.");
		}
		else if(args[0].equalsIgnoreCase("tab"))
		{
			if(args.length < 2)
			{
				sender.sendMessage("Usage: /ope tab [Header],,,[Footer]");
				return true;
			}
			
			String arg = args[1];
			
			for(int i = 2; i < args.length; i++)
				arg += " " + args[i];
			
			String[] parts = arg.split(",,,", 2);
			String header = ChatColor.translateAlternateColorCodes('&', parts[0]);
			String footer = ChatColor.translateAlternateColorCodes('&', parts[1]);
			
			TabHeader.send(header, footer, (Player) sender);
			sender.sendMessage("Sent.");
		}
		else if(args[0].equalsIgnoreCase("title"))
		{
			if(args.length < 2)
			{
				sender.sendMessage("Usage: /ope title [Title],,,[Subtitle]");
				return true;
			}
			
			String arg = args[1];
			
			for(int i = 2; i < args.length; i++)
				arg += " " + args[i];
			
			String[] parts = arg.split(",,,", 2);
			String title = ChatColor.translateAlternateColorCodes('&', parts[0]);
			String subtitle = ChatColor.translateAlternateColorCodes('&', parts[1]);
			
			Title.send((Player) sender, title, subtitle);
			sender.sendMessage("Sent.");
		}
		
		return true;
	}
}
